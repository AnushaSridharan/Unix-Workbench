echo "Let's play a game of guessing the number of files in the present directory."
echo "Enter your guess:"
#Take the input from the user
read a
#count the number of files in the present directory
count=$(ls -1 | wc -l)
# Check if the value what user entered with the number of files
#Ask the user to keep guessing until the correct value is entered
function guessgame() {
while [ $a -ne $count ]
do
if [ $a -gt $count ]
then
echo "Too High, guess again"
elif [ $a -le $count ]
then
echo "Too low, guess again"
fi
read a
done
}
#Calling the function - guessgame
guessgame
# The loops ends when the user enters the correct value and congratulate. 
echo "Congratulations, you guessed it right!"
make 
